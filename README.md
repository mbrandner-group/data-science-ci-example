## Getting Started

- See [Getting Started With CI/CD for Data Science Pipelines](https://handbook.gitlab.com/handbook/business-technology/data-team/platform/ci-for-ds-pipelines/) in the Gitlab Handbook for additional information and setup instructions.

- See [Data Science / ML Component Pipeline)](https://gitlab.com/explore/catalog/gitlab-data/ds-component-pipeline) in the [CI/CD Catalog](https://gitlab.com/explore/catalog) for more information on how the [.gitlab-ci.yml](https://gitlab.com/gitlab-data/data-science-ci-example/-/blob/main/.gitlab-ci.yml?ref_type=heads) is configured.

- Uses [gitlabds](https://pypi.org/project/gitlabds/) package to easily commute standard model metrics.

- The [sample training notebook](https://gitlab.com/gitlab-data/data-science-ci-example/-/blob/main/notebooks/training_example.ipynb?ref_type=heads) provided uses [xgboost](https://xgboost.readthedocs.io/en/stable/) with [Optuna](https://github.com/optuna/optuna-examples/) and the [sklearn breast cancer](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html) dataset.

- The [sample scoring notebook](https://gitlab.com/gitlab-data/data-science-ci-example/-/blob/main/notebooks/scoring_example.ipynb?ref_type=heads) provided scores the [sklearn breast cancer](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html) dataset using the [xgboost](https://xgboost.readthedocs.io/en/stable/)) created from the above training notebook. 